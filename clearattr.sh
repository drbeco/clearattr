#!/bin/bash

# **************************************************************************
# * (C)opyright 2023         by Ruben Carlo Benante                        *
# *                                                                        *
# * This program is free software; you can redistribute it and/or modify   *
# *  it under the terms of the GNU General Public License as published by  *
# *  the Free Software Foundation version 2 of the License.                *
# *                                                                        *
# * This program is distributed in the hope that it will be useful,        *
# *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *  GNU General Public License for more details.                          *
# *                                                                        *
# * You should have received a copy of the GNU General Public License      *
# *  along with this program; if not, write to the                         *
# *  Free Software Foundation, Inc.,                                       *
# *  59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
# *                                                                        *
# * Contact author at:                                                     *
# *  Ruben Carlo Benante                                                   *
# *  rcb@beco.cc                                                           *
# **************************************************************************

Help()
{
    cat << EOF
    clearattr - Clear ACL fields
    Usage:
           ${0} [-h|-V]
           ${0} [-v] [-r dir] [-d] -n FIELD
           ${0} [-v] [-f dir|file] [-d] -n FIELD

    Options:
      -h, --help       Show this help.
      -V, --version    Show version.
      -v, --verbose    Turn verbose mode on (cumulative).
      -q, --quiet      Turn verbose off.

      -r, --recursive  Recursively enter directories (arg must be directory).
      -f, --file       Examine single file or directory.

      -n, --name       Remove only -n FIELD field.
                       -n all    : remove all ACL fields.
                       -n filter : remove only matcheds fields.
      -d, --dryrun     Just print what would be removed, but do not remove.

    Exit status:
       0, if ok.
       1, some error occurred.


    Note: it will remove only ACL fields, not ACL permissions. 
    For ACL fields, use getfattr and setfattr family commands. 
    For ACL permissions, use getfacl and setfacl family of commands.

    Todo: Long options not implemented yet.

    Author: Written by Ruben Carlo Benante <rcb@beco.cc>

EOF
    exit 1
}

Copyr()
{
    echo 'clearattr - 20230417.105035'
    echo
    echo 'Copyright (C) 2023 Ruben Carlo Benante <rcb@beco.cc>, GNU GPL version 2'
    echo '<http://gnu.org/licenses/gpl.html>. This  is  free  software:  you are free to change and'
    echo 'redistribute it. There is NO WARRANTY, to the extent permitted by law. USE IT AS IT IS. The author'
    echo 'takes no responsability to any damage this software may inflige in your data.'
    echo
    exit 1
}

# echo if verbose
echov()
{
    NUM="$1"
    MSG="${@:2}"
    if [ "$verbose" -ge "$NUM" ] ; then
        echo "$MSG"
    fi
}

# Example of a function
main()
{
    verbose=1
    ITSDIR=0
    DIRFILE=""
    RECURSIVE=2
    FIELDS="none"
    DRYRUN=0
    SDRY=""
    #getopt example with switch/case
    while getopts "hVvqr:f:n:d" FLAG; do
        case $FLAG in
            h)
                Help
                ;;
            V)
                Copyr
                ;;
            v)
                let verbose=verbose+1
                ;;
            q)
                let verbose=0
                ;;
            r)
                DIRFILE=$OPTARG
                RECURSIVE=1
                ;;
            f)
                DIRFILE=$OPTARG
                RECURSIVE=0
                ;;
            n)
                FIELDS=$OPTARG
                ;;
            d)
                DRYRUN=1
                SDRY="-d"
                ;;
            *)
                Help
                ;;
        esac
    done

    if [ "$verbose" -gt 1 ] ; then
        let verbose=verbose-1
    fi

    echov 2 Starting clearattr.sh script, by root, version 20230417.105035...
    echov 3 Verbose level: $verbose

    if [ "$RECURSIVE" -eq 2 ] ; then
        echo 'You must use one of the args: -r dir or -f dir|file'
        exit 1
    fi

    if [ -d "$DIRFILE" ]; then
        ITSDIR=1
        echov 3 Arg is a directory
    fi

    if [ "$RECURSIVE" -eq 1 -a "$ITSDIR" -eq 0 ] ; then
        echo 'Use of --recursive limited to directories'
        exit 1
    fi

    if [ "$FIELDS" == "none" ] ; then
        echov 1 "Missing -n FIELD. Running dry: -d -n all."
        FIELDS="all"
        DRYRUN=1
        SDRY="-d"
    fi
    if [ "$DRYRUN" -eq 1 ] ; then
        echov 2 "Dry-run, removing nothing" 
    fi

    if [ "$RECURSIVE" -eq 0 ] ; then # single file
        while read F ; do
            # At the end, break the loop
            if [[ -z "$F" ]] ; then
                break;
            fi
            # echo "Field: $F"
            # Do not use the filename as field
            if [[ "$F" =~ ^#[[:space:]]file:[[:space:]] ]] ; then
                echov 1 "$F"
                continue
            fi
            # skip system.posix_acl_access, as this is a marker for ACL permissions
            # to remove this one, use setfacl -bk file
            if [[ "$F" =~ ^system.posix_acl_access ]] ; then
                echov 2 "Skip ACL $F"
                continue
            fi

            # none, all, some logic
            # if [ "$FIELDS" == "none" ] ; then # dry-run, just print fields
            #     echo "will remove: $F"
            if [ "$FIELDS" == "all" ] ; then # all, remove all fields (careful! check first. Maybe confirm each one)
                if [ "$DRYRUN" -eq 1 ] ; then
                    echov 1 "Will remove: $F"
                else
                    echov 1 "Removing: $F"
                    sudo setfattr -x "$F" "$DIRFILE"
                fi
            else # remove only the ones that match
                if grep -q "^$FIELDS" <<< "$F" ; then
                    if [ "$DRYRUN" -eq 1 ] ; then
                        echov 1 "Will remove: $F"
                    else
                        echov 1 "Removing: $F"
                        sudo setfattr -x "$F" "$DIRFILE"
                    fi
                fi
            fi
        done < <(sudo getfattr -m- "$DIRFILE")

        exit 0
    fi

    # Recursive
    # find thedir -exec ~/tmp/xacl/clearattr.sh -vd -r {} -n all \;
    SVERB=$(printf '%*s' $verbose | tr ' ' "v")
    SVERB="-"$SVERB
    if [ "$SVERB" == "-" ] ; then
        SVERB="-q"
    fi
    echov 4 Verbose level $SVERB
    echov 4 RECURSIVE COMMAND: find "$DIRFILE" -type f,d -exec "${0}" "$SDRY" "$SVERB" -f {} -n "$FIELDS" \;
    find "$DIRFILE" -type f,d -exec "${0}" "$SDRY" "$SVERB" -f {} -n "$FIELDS" \;

    exit 0
    echo Bye main
}

#Calling main with all args
main "$@"
echo Bye script
exit 0

#/* -------------------------------------------------------------------------- */
#/* vi: set ai et ts=4 sw=4 tw=0 wm=0 fo=croql syn=sh : SH config Vim modeline */
#/* Template by Dr. Beco <rcb at beco dot cc>          Version 20190318.122053 */


